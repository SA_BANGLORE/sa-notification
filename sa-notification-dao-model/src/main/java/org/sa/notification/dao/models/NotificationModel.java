package org.sa.notification.dao.models;

import javax.persistence.*;

/**
 * Created by Vivek on 19-01-2017.
 */
@Entity
@Table(name = NotificationModel.TBL_NOTIFICATION)
public class NotificationModel extends AbstractPersistable {

    public static final String TBL_NOTIFICATION = "TBL_NOTIFICATION";

    @Column(name="NOTIFICATION_ID")
    private String notificationId;

    @Column(name="DONOR_ID")
    private String donorId;

    @Column(name="SA_ID")
    private String saId;

    @Enumerated(EnumType.STRING)
    @Column(name="NOT_STATUS")
    private NotificationStatus notificationStatus;

    @Enumerated(EnumType.STRING)
    @Column(name="NOT_TYPE")
    private NotificationType notificationType;

    @Enumerated(EnumType.STRING)
    @Column(name="NOT_ACTION")
    private NotificationAction notificationAction;

    @Column(name="DONATION_AMOUNT")
    private Double donatedAmount;

    public Double getDonatedAmount() {
        return donatedAmount;
    }

    public void setDonatedAmount(Double donatedAmount) {
        this.donatedAmount = donatedAmount;
    }

    public String getDonorId() {
        return donorId;
    }

    public void setDonorId(String donorId) {
        this.donorId = donorId;
    }

    public NotificationAction getNotificationAction() {
        return notificationAction;
    }

    public void setNotificationAction(NotificationAction notificationAction) {
        this.notificationAction = notificationAction;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getSaId() {
        return saId;
    }

    public void setSaId(String saId) {
        this.saId = saId;
    }
}
