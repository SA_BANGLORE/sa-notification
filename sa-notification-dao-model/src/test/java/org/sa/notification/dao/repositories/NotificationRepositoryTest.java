package org.sa.notification.dao.repositories;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sa.notification.dao.DaoConfig;
import org.sa.notification.dao.TestApplication;
import org.sa.notification.dao.models.NotificationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by Vivek on 20-01-2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
@DatabaseSetup({"postal-address-init.xml", "notification-init.xml"})
@SpringBootTest(classes = {TestApplication.class, DaoConfig.class})
public class NotificationRepositoryTest {

    @Autowired
    private NotificationRepository notificationRepository;


    @After
    @DatabaseTearDown
    public void tearDown() throws Exception {

    }

    @Test()
    public void testSave() {
        NotificationModel entity = new NotificationModel();
        NotificationModel result = notificationRepository.save(entity);
        assertThat("Failed to persist object", result, notNullValue());
        assertThat("Auto generated Id failed", result.getId(), notNullValue());
    }
}
