#!/usr/bin/env bash

TOKEN=$(curl -X POST --noproxy localhost -u web-client:web-secret 'localhost:8080/oauth/token?grant_type=password&client_id=web-client&client_secret=web-secret&scope=read&username=test&password=password' -H "Accept: application/json" | egrep --color=never -o '[a-f0-9-]{20,}')
echo "Got token $TOKEN"
curl --noproxy localhost localhost:8082/donors/1 -H "Authorization: Bearer $TOKEN"
echo ''
