package org.sa.donor.app.controllers;

import org.sa.notification.api.model.CashDonation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author neermitt.
 */
@RestController
@RequestMapping("/donation/")
public class NotificationController {

    @RequestMapping(path = "cash/", method = RequestMethod.PUT)
    public CashDonation acceptDonation(@RequestBody CashDonation.Create cashDonation) {
        // TODO: Resolve donor
        // TODO: Accept donation

        return null;
    }

    @RequestMapping(path = "cash/my", method = RequestMethod.GET)
    public List<CashDonation> getDonations() {
        return null;
    }
}
