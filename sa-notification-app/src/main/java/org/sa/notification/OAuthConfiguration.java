package org.sa.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * @author neermitt.
 */
//@Configuration
//@EnableResourceServer
public class OAuthConfiguration extends ResourceServerConfigurerAdapter {


    @Autowired
    private ResourceServerProperties resource;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(this.resource.getResourceId());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/profile/**").access("#oauth2.hasScope('read_profile')")
            .antMatchers("/**").access("#oauth2.hasScope('read_profile')");
    }

}
