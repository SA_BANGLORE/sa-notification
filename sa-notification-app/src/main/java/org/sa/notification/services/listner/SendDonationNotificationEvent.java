package org.sa.notification.services.listner;

import org.sa.notification.api.CashDonation;
import org.springframework.context.ApplicationEvent;

/**
 * @author neermitt.
 */
public class SendDonationNotificationEvent extends ApplicationEvent {
    private final String agentId;
    private final CashDonation cashDonation;

    /**
     * Create a new ApplicationEvent.
     *
     * @param source       the object on which the event initially occurred (never {@code null})
     * @param agentId
     * @param cashDonation
     */
    public SendDonationNotificationEvent(Object source, String agentId, CashDonation cashDonation) {
        super(source);
        this.agentId = agentId;
        this.cashDonation = cashDonation;
    }

    public String getAgentId() {
        return agentId;
    }

    public CashDonation getCashDonation() {
        return cashDonation;
    }
}
