package org.sa.notification.services.listner;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.sa.notification.services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;

/**
 * Created by Vivek on 05-03-2017.
 */

public class UserEventsProcessor implements ApplicationListener<SendDonationNotificationEvent> {

    @Autowired
    private NotificationService notificationService;


    public void onApplicationEvent(SendDonationNotificationEvent event) {
        SendDonationNotificationEvent donationNotificationEvent = (SendDonationNotificationEvent) event;

        try {
            notificationService.generateNotification(donationNotificationEvent);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        // Do more processing as per application logic
    }
}
