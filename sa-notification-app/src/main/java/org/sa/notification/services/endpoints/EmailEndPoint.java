package org.sa.notification.services.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * Created by Vivek on 22-02-2017.
 */
@Service
public class EmailEndPoint {

    @Autowired
    private JavaMailSender javaMailService;


    public void sendEmail(String email,String subject,String content){
        SimpleMailMessage mailMessage=new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("Swaraj Abhiyan Donation Update");
        mailMessage.setText(content);
        javaMailService.send(mailMessage);
        System.out.println("mail is sent");
    }
}
