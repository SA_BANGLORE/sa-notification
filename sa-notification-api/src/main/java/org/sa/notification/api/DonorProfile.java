package org.sa.notification.api;

/**
 * @author neermitt.
 */
public class DonorProfile {
    public Long id;

    public Name name;

    public Gender gender;

    public String mobile;

    public String email;

    /** This is the id of the agent **/
    public Long agentId;

    public static class Summary {
        public long id;

        public Name name;
    }

    public static class Detail extends Summary {
        public Gender gender;

        public String mobile;

        public String email;
    }
}
