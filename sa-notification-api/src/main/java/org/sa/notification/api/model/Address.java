package org.sa.notification.api.model;

/**
 * @author neermitt.
 */
public class Address {
    public String Country;
    public String State;
    public String City;
    public String PostalCode;
    public String addressLine1;
    public String addressLine2;
}
