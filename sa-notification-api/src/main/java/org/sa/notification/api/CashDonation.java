package org.sa.notification.api;

import java.util.Date;

/**
 * Created by Vivek on 05-03-2017.
 */

public class CashDonation {
    public long donationId;
    public String donationStatus;
    public DonorProfile.Summary donor;
    public double amount;
    public String agentName;
    public Date date;

    public static class Create {
        public DonorProfile donor;
        public double amount;
        public String comments;
        public int finYear;
    }
}
