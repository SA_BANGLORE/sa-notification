package org.sa.notification.api.model;

/**
 * @author neermitt.
 */
public class CashDonation {
    public int finYear;
    public long donationId;

    public DonorProfile.Summary donor;

    public double amount;

    public static class Create {
        public DonorProfile donor;
        public double amount;
    }
}
