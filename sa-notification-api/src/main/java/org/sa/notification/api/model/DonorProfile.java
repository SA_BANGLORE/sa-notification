package org.sa.notification.api.model;

import java.util.Map;

/**
 * @author neermitt.
 */
public class DonorProfile {
    public long id;

    public Name name;

    public Address address;

    public Gender gender;

    public String mobile;

    public String email;

    public Map<String, String> identities;

    public Map<String, Object> properties;


    public static class Search {
        public String mobile;
        public String email;
    }

    public static class Summary {
        public long id;

        public Name name;
    }
}
