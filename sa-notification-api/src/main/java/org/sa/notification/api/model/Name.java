package org.sa.notification.api.model;

/**
 * @author neermitt.
 */
public class Name {
    public String first;
    public String middle;
    public String last;
}
