package org.sa.notification.api.model;

/**
 * @author neermitt.
 */
public enum Gender {
    Male,
    Female,
    Other
}
